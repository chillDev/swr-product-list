import { FC, useState } from 'react';
import useSWR from 'swr';
import './App.css';

import { Photo, PhotoData } from './types';

const App: FC = () => {
  const [photos, setPhotos] = useState<Photo[]>([]);

  const { data, error, mutate } = useSWR<PhotoData[]>(
    'https://jsonplaceholder.typicode.com/photos',
    async (url) => {
      const res = await fetch(url);
      const data = await res.json();
      return data;
    }
  );

  const handleAddPhoto = () => {
    setPhotos([
      ...photos,
      { id: photos.length + 1, title: `Photo ${photos.length + 1}` },
    ]);
  };

  // const handleAddPhoto = () => {
  //   setPhotos([
  //     ...photos,
  //     { id: photos.length + 1, title: `Photo ${photos.length + 1}` },
  //   ]);
  //   mutate([
  //     ...data,
  //     {
  //       id: data.length + 1,
  //       title: `Photo ${data.length + 1}`,
  //       url: 'https://via.placeholder.com/150',
  //       thumbnailUrl: 'https://via.placeholder.com/150',
  //     },
  //   ]);
  // };

  return (
    <div className='AppContainer'>
      <button onClick={handleAddPhoto} className='addButton'>
        Add photo
      </button>
      <div className='photoWrapper'>
        {error && <div>Error: {error.message}</div>}
        <div className='photos'>
          {photos.map((photo) => (
            <div key={photo.id} className='photoContainer'>
              <h3>{photo.title}</h3>
              <div className='photo'>
                {data && data.length > 0 ? (
                  <img
                    src={data[photo.id - 1].thumbnailUrl}
                    alt={photo.title}
                  />
                ) : (
                  <p>Loading...</p>
                )}
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default App;
