export interface Photo {
  id: number;
  title: string;
}

export interface PhotoData {
  id: number;
  title: string;
  url: string;
  thumbnailUrl: string;
}
